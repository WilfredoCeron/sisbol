<div class="card card-info col-8">
  <div class="card-header">
    <h3 class="card-title">Datos Personales</h3>
  </div>
    <form method="post" action="<?= base_url() ?>/Postulante/form_validation" class="form-horizontal">
      <div class="card-body">
        <div class="form-group row">
          <p  class="col-sm-2 col-form-label">Nombre<span>*</span></p>
          <div class="col-7">
            <input type="text" class="form-control" placeholder="escribe tu nombre">
          </div>
        </div>
        <div class="form-group row">
          <p  class="col-sm-2 col-form-label">Apellido<span>*</span></p>
          <div class="col-7">
            <input type="text" class="form-control" placeholder="escribe tus apellidos">
          </div>
        </div>
        <div class="form-group row">
        <p  class="col-sm-2 col-form-label">Identificación<span>*</span></p>
          <div class="col-4">
            <select class="form-control select2" style="width: 100%;">
              <option selected="selected">--- seleccionar ---</option>
              <option>Pasaporte</option>
              <option>celuda de extrangeria</option>
              <option>Documento Unico de Identidad - DUI</option>
              <option>Numero Unico Tributario</option>
            </select>
          </div>
          <div class= col-3>
            <input type="text" class="form-control" placeholder="identificación">
          </div>
        </div>
        <div class="form-group row">
        <p  class="col-sm-2 col-form-label">Fecha de Nacimiento<span>*</span></p>
          <div class="col-7 row">
            <div class="col-3">
              <select class="form-control select2" style="width: 100%;">
                <option selected="selected">Dia</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
              </select>
            </div>
            <div class="col-5">
              <select class="form-control select2" style="width: 100%;">
                <option selected="selected">Mes</option>
                <option value="ene">Enero</option>
                <option value="feb">Febrero</option>
                <option value="mar">Marzo</option>
                <option value="abr">Abril</option>
                <option value="may">Mayo</option>
                <option value="jun">Junio</option>
                <option value="jul">Julio</option>
                <option value="ago">Agosto</option>
                <option value="sep">Septiembre</option>
                <option value="oct">Octubre</option>
                <option value="nov">Noviembre</option>
                <option value="dic">Diciembre</option>
              </select>
            </div>
            <div class="col-4">
            <select class="form-control select2" style="width: 100%;">
                <option selected="selected">Año</option>
                <option value="90">1990</option>
                <option value="91">1991</option>
                <option value="92">1992</option>
                <option value="93">1993</option>
                <option value="94">1994</option>
                <option value="95">1995</option>
                <option value="96">1996</option>
                <option value="97">1997</option>
                <option value="98">1998</option>
                <option value="99">1999</option>
                <option value="00">2000</option>
                <option value="01">2001</option>
              </select>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <p  class="col-sm-2 col-form-label">Genero<span>*</span></p> 
          <div class="col-7">
            <div class="form-group clearfix">
              <div class="icheck-primary d-inline">
                <input type="radio" id="radioPrimary1" name="r1" checked>
                <label class="form-check-label">Masculino</label>
              </div>
              <div class="icheck-primary d-inline">
                <input type="radio" id="radioPrimary2" name="r1">
                <label class="form-check-label">Femenino</label>
              </div>
            </div>
          </div>
        </div>
        <div class="form_group row">
        <p  class="col-sm-2 col-form-label">Estado Civil<span>*</span></p>
            <div class="col-7">
              <div class="form_inputs">
                <select class="form-control select2" style="width: 100%;">
                  <option value="0">Estado Civil</option>
                  <option value="1" selected >Soltero(a)</option>
                  <option value="2">Casado(a)</option>
                  <option value="3">Separado(a)/Divorcia</option>
                  <option value="4">Viudo(a)</option>
                  <option value="5">Unión libre</option>
                </select>
              </div>
            </div>
        </div>
        <br>
        <div class="form-group row">
        <p  class="col-sm-2 col-form-label">Telefono 1<span>*</span></p>
          <div class="col-7 row">
              <div class="col-4">
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">seleccionar</option>
                  <option value="fi">fijo</option>
                  <option value="ce">celular</option>
                  <option value="of">oficina</option>
                </select>
              </div>
              <div class="col-4">
                <input type="text" class="form-control" placeholder="Cod de pais">
              </div>
              <div class="col-4">
                <input type="text" class="form-control" placeholder="7777-7777">
              </div>
          </div>
        </div>
        <div class="form-group row">
        <p  class="col-sm-2 col-form-label">Telefono 2</p>
          <div class="col-7 row">
              <div class="col-4">
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">seleccionar</option>
                  <option value="fi">fijo</option>
                  <option value="ce">celular</option>
                  <option value="of">oficina</option>
                </select>
              </div>
              <div class="col-4">
                <input type="text" class="form-control" placeholder="Cod de pais">
              </div>
              <div class="col-4">
                <input type="text" class="form-control" placeholder="7777-7777">
              </div>
          </div>
        </div>
        <div class="form-group row">
          <p  class="col-sm-2 col-form-label">Email</p>
                    <div class="col-sm-7">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="example@example.com">
                    </div>
                  </div>
        <div class="form_group row">
        <p  class="col-sm-2 col-form-label">Pais<span>*</span></p>
            <div class="col-7">
              <div class="form_inputs">
                <select class="form-control select2" style="width: 100%;">
                  <option value="0">Brasil</option>
                  <option value="1" selected >El Salvador</option>
                  <option value="2">Guatemala</option>
                  <option value="3">Nicaragua</option>
                  <option value="4">España</option>
                  <option value="5">Panama</option>
                </select>
              </div>
            </div>
        </div>
        <br>
        <div class="form_group row">
          <p class="col-sm-2 col-form-label">Departamento<span>*</span></p>
            <div class="col-7">
              <div class="form_inputs">
                <select class="form-control select2" style="width: 100%;">
                  <option value="0">San Salvador</option>
                  <option value="1" selected >Chalatenango</option>
                  <option value="2">Morazan</option>
                  <option value="3">San Vicente</option>
                  <option value="4">La Union</option>
                  <option value="5">Sonsonate</option>
                </select>
              </div>
            </div>
        </div>
        <br>
        <div class="form_group row">
          <p class="col-sm-2 col-form-label">Cuidad<span>*</span></p>
            <div class="col-7">
              <div class="form_inputs">
                <select class="form-control select2" style="width: 100%;">
                  <option value="0"  selected>Soyapango</option>
                  <option value="1" >Los Santos 1</option>
                  <option value="2">Los Santos 2</option>
                  <option value="3">...</option>
                  <option value="4">...</option>
                  <option value="5">...</option>
                </select>
              </div>
            </div>
        </div>
        <br>
        <div class="form_group row">
          <p class="col-sm-2 col-form-label">Dirección<span>*</span></p>
            <div class="col-7">
            <input type="text" class="form-control" placeholder="Escribe tu direccion completa">
          </div>
        </div>
        <br>
        <div class="form-group row">
          <p  class="col-sm-2 col-form-label">Licencia de conducir<span>*</span></p>
          <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Automovil </label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label"> Motocicleta </label>
                        </div>
              </div>
        </div>
        <div class="form-group row">
          <p  class="col-sm-2 col-form-label">Discapacidad<span>*</span></p> 
          <div class="col-7">
            <div class="form-group clearfix">
              <div class="icheck-primary d-inline">
                <input type="radio" id="radioPrimary1" name="r1" checked>
                <label class="form-check-label">si</label>
              </div>
              <div class="icheck-primary d-inline">
                <input type="radio" id="radioPrimary2" name="r1">
                <label class="form-check-label">no</label>
              </div>
            </div>
          </div>
        </div>

        </div>
        
        <div class="form_group row ">
        <div class="col-md-offset-4 col-7">
        <button type="submit" class="btn btn-info">Guardar</button>
        </div>
        </div> 
      </div>
      
    </form>
</div>
