<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= site_url('/assents/css/adminlte.min.css'); ?>" />
    <link rel="stylesheet" href="<?= site_url('/assents/css/all.min.css'); ?>" />
    <!--<link rel="stylesheet" href="http://code.ionicframework.com/1.3.3/css/ionic.min.css" />-->
</head>
<body>
   <header>
    <div class="card bg-info col-12">
        <div class="card-header">
            <h2>Bolsa de Trabajo en Linea - SISBOL</h2>
        </div>
        <nav class="site-header sticky-top py-1">
            <div class="card-body container d-flex flex-column flex-md-row justify-content-between">
                <a class="py-2" href="#" aria-label="Product">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="d-block mx-auto  text-white " role="img" viewBox="0 0 24 24" focusable="false"><title>Product</title><circle cx="12" cy="12" r="10"/><path d="M14.31 8l5.74 9.94M9.69 8h11.48M7.38 12l5.74-9.94M9.69 16L3.95 6.06M14.31 16H2.83m13.79-4l-5.74 9.94"/></svg>
                </a>
                <a class="py-2 d-none d-md-inline-block text-white  " href="#">Mi Area</a>
                <a class="py-2 d-none d-md-inline-block  text-white " href="#">Mis Postulantes</a>
                <a class="py-2 d-none d-md-inline-block  text-white " href="#">Buscar Ofertas</a>
                <a class="py-2 d-none d-md-inline-block  text-white " href="#">Mi Curriculum</a>
                <a class="py-2 d-none d-md-inline-block  text-white " href="#">Mis Alertas</a>
                <a class="py-2 d-none d-md-inline-block  text-white " href="#">Configuraciones </a>
            </div>
        </nav>
        </div>    
   </header>
   <h1><?= esc($title); ?></h1>